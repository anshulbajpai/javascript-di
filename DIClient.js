ServiceDependency = Class({	
	constructor : function(param1, param2){
		this.param1 = param1;
		this.param2 = param2;
	},	
	helloWorld : function(){
		console.log(this.param1 + this.param2);
	}	
}).create();

ServiceA = Class({	
	constructor : function(dependency){
		this.dependency = dependency;
	},	
	helloWorld : function(){
		this.dependency.helloWorld();
	}	
}).create();

ServiceB = Class({	
	constructor : function(dependency){
		this.dependency = dependency;
	},	
	helloWorld : function(){
		this.dependency.helloWorld();
	}	
}).create();

DI.register({serviceDependency : [ServiceDependency, "Hello ", "World"]})
  .register({serviceA : [ServiceA, DI.serviceDependency],
			 serviceB : [ServiceB, DI.serviceDependency]});


DI.serviceA.helloWorld();
DI.serviceB.helloWorld();