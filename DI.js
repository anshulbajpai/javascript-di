DI = Class({
	register : function(beans){
		for(var beanKey in beans){
			var beanConfig = beans[beanKey];
			this[beanKey] = this._create(beanConfig.shift(), beanConfig) 
		}
		return this;
	},
	_create : function(klass, args){
		function Dummy(){
			return klass.apply(this, args);
		}
		Dummy.prototype = klass.prototype;
		return new Dummy();
	}
}).create();

var DI = new DI();